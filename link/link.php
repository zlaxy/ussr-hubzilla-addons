<?php
/**
 * Name: Link
 * Description: Using short links on whole hub
 * Version: 0.87
 * Author: ivan zlax <@zlax@ussr.win>
 * Maintainer: ivan zlax <@zlax@ussr.win>
 */

use Zotlabs\Lib\Apps;
use Zotlabs\Extend\Hook;
use Zotlabs\Extend\Route;

function link_load() {
	Route::register('addon/link/Mod_Link.php','link');
	if (get_config('link','linkonhub'))
		register_hook('dropdown_extras', 'addon/link/link.php', 'dropdown_link');
	if (get_config('link','hideasides'))
		register_hook('page_header', 'addon/link/link.php', 'hideasides_header');
	set_config('link','linkenabled',1);
}

function link_unload() {
	Route::unregister('addon/link/Mod_Link.php','link');
	unregister_hook('dropdown_extras', 'addon/link/link.php', 'dropdown_link');
	unregister_hook('page_header', 'addon/link/link.php', 'hideasides_header');
	del_config('link','linkenabled');
}

function link_plugin_admin(&$a) {
	$t = get_markup_template( "admin.tpl", "addon/link/" );
	$a = replace_macros($t, array(
		'$submit' => t('Submit'),
		'$hideasides' => array('hideasides', t('Hide side panels on the link page'), get_config('link', 'hideasides')),
		'$linkonhub' => array('linkonhub', t('Show "Link on Hub" in the dropdown menu'), get_config('link', 'linkonhub')),
	));
}

function link_plugin_admin_post() {
	set_config('link','linkonhub',trim($_POST['linkonhub']));
	set_config('link','hideasides',trim($_POST['hideasides']));
	if (get_config('link','linkonhub'))
		register_hook('dropdown_extras', 'addon/link/link.php', 'dropdown_link');
	else
		unregister_hook('dropdown_extras', 'addon/link/link.php', 'dropdown_link');
	if (get_config('link','hideasides'))
		register_hook('page_header', 'addon/link/link.php', 'hideasides_header');
	else
		unregister_hook('page_header', 'addon/link/link.php', 'hideasides_header');
	info( t('Settings updated.') . EOL );
}

function dropdown_link(&$extras) {
	$arr = $extras;
	$item_id = $extras['item']['item_id'];
	$arr['dropdown_extras'] .= '<a class="dropdown-item" href="'.z_root().'/link/'.$item_id.'" title="'.t('Link on Hub').'" class="u-url"><i class="generic-icons-nav fa fa-fw fa-link"></i>'.t('Link on Hub').'</a>';
	$extras = $arr;
}

function hideasides_header(&$b) {
	$querystr = stristr(App::$query_string, '/', true);
	if ($querystr == "link") {
		$addScriptTag = "<style>#region_1 { display: none !important; } #region_3 { display: none !important; }</style>\r\n";
		App::$page['htmlhead'] .= $addScriptTag;
	}
}
