Short Link Addon
================================

Using short links on whole hub.

With this add-on enabled, it is possible to open posts and comments by the address:

hub.domain/link/item

where an item is the numeric value of an object from the database.

It is possible to use /admin/addons/link settings page for enabling or disabling of showing "Link on Hub" in the dropdown menu.

Based on Zotlabs/Module/Display.php

Adapted for Hubzilla 8.8